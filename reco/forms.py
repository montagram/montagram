from django import forms
from PIL import Image
import imghdr

class UploadImageForm(forms.Form):
    picture = forms.ImageField()


    def clean_picture(self):
        data = self.cleaned_data['picture']
        if imghdr.what(data) == 'gif':
            raise forms.ValidationError(".gif extension is not supported.")
        return data


class ContribImageForm(forms.Form):
    name = forms.ChoiceField()
    other = forms.CharField(required=False)
    picture = forms.ImageField()

    def clean_picture(self):
        data = self.cleaned_data['picture']
        if imghdr.what(data) == 'gif':
            raise forms.ValidationError(".gif extension is not supported.")
        return data
        
    def clean(self):
        cleaned_data = super(ContribImageForm, self).clean()
        name = cleaned_data.get("name")
        other = cleaned_data.get("other")
        if name == '0' and not other:
            raise forms.ValidationError("Other is a required field if selected in Name.")
        return cleaned_data


class CropFileForm(forms.Form):
    x1 = forms.DecimalField()
    x2 = forms.DecimalField()
    y1 = forms.DecimalField()
    y2 = forms.DecimalField()
