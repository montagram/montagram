from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, render, redirect
from reco.forms import ContribImageForm, CropFileForm, UploadImageForm
from django.template import RequestContext
from reco.models import Mountpic, Mountain
from django.core.urlresolvers import reverse
from PIL import Image
from django import forms
from django.core.files.base import ContentFile
from client import sendjson


def index(request):
    return redirect('/')


def choice(request):
    return render(request, 'reco/choice.html')    


def upload(request):
    form = UploadImageForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        mountpic = Mountpic()
        mountpic.picture = form.cleaned_data['picture']
        mountpic.save()
        return HttpResponseRedirect(reverse('reco:crop', args=(mountpic.id,)))
    return render_to_response('reco/upload.html', {'form': form}, context_instance=RequestContext(request))


def contrib(request):
    form = ContribImageForm(request.POST or None, request.FILES or None)
    form.fields['name'] = forms.ChoiceField(choices=get_mountains_name())
    if form.is_valid():
        mountpic = Mountpic()
        if form.cleaned_data['name'] !='0':
            mount = Mountain.objects.get(id=form.cleaned_data['name'])
        else:
            mount = Mountain()
            mount.name = form.cleaned_data['other']
        mountpic.picture = form.cleaned_data['picture']
        mount.save()
        mountpic.mount = mount
        mountpic.save()
        return HttpResponseRedirect(reverse('reco:crop', args=(mountpic.id,)))
    return render_to_response('reco/contrib.html', {'form': form}, context_instance=RequestContext(request))


def crop(request, image_id):
    mountpic = Mountpic.objects.get(id=image_id)
    form = CropFileForm(request.POST or None, request.FILES or None, auto_id=True)
    if form.is_valid():
        x1 = int(round(float(form.cleaned_data['x1']), 0))
        x2 = int(round(float(form.cleaned_data['x2']), 0))
        y1 = int(round(float(form.cleaned_data['y1']), 0))
        y2 = int(round(float(form.cleaned_data['y2']), 0))
        crop_coords = (x1, y1, x2, y2)
        mountpic.croppic.save('crop.jpg', ContentFile(cropper(mountpic.picture.url, crop_coords)), save=True)
        """ if a mountain has a name, it came from contrib"""
        if hasattr(mountpic.mount, 'name'):
            return render(request, "reco/thanks.html")
        else:
            #return render(request, "reco/result.html", sendjson(message))
            return HttpResponseRedirect(reverse('reco:result', args=(mountpic.id,)))

    return render_to_response('reco/crop.html', {'form': form, 'image_id': image_id, 'image': mountpic.picture}, context_instance=RequestContext(request))


def get_mountains_name():
    mountains = Mountain.objects.all()
    mountlist = []
    for mount in mountains:
        mountlist.append((mount.id, mount.name))
    mountlist.append((0, 'Other'))
    return mountlist


def cropper(original_image_url, crop_coords):
    """ Open original, create and return  a new cropped image"""
    import urllib2
    from cStringIO import StringIO
    input_file = StringIO(urllib2.urlopen(original_image_url).read())
    input_file.seek(0)
    img = Image.open(input_file).crop(crop_coords)
    tmp = StringIO()
    img.save(tmp, 'JPEG', )
    tmp.seek(0) 
    input_file.close()
    return tmp.getvalue()


def presentation(request):
    return redirect("http://montagram.insa-lyon.fr")


def result(request, image_id):
    mountpic = Mountpic.objects.get(id=image_id)
    message = {'picture': mountpic.croppic.url, 'name': 'not_defined'}
    result = sendjson(message)
    return render_to_response('reco/result.html', {'image_id': image_id, 'image': mountpic.picture, 'name': result['name'],
                                                   'accuracy1': result['accuracy1'], 'accuracy2': result['accuracy2'], 'accuracy3': result['accuracy3']},
                              context_instance=RequestContext(request))


def thanks(request):
    return render(request, 'reco/thanks.html')


def contact(request):
    return render(request, 'reco/contact.html')
