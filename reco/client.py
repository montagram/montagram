import socket
import json
from django.conf import settings

def sendjson(data):
    """a simple tcp client that send and receive json """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('54.198.130.83', 13373))
    #s.connect(('127.0.0.1', 13373))
    s.send(json.dumps(data))
    result = json.loads(s.recv(1024))
    s.close()
    return result
