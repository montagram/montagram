from django.conf.urls import patterns, url

from reco import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^upload/$', views.upload, name='upload'),
    url(r'^crop/(\d+)/$', views.crop, name='crop'),
    url(r'^choice/$', views.choice, name='choice'),
    url(r'^contrib/$', views.contrib, name='contrib'),
    url(r'^presentation/$', views.presentation, name='presentation'),
    url(r'^result/(\d+)/$', views.result, name='result'),
    url(r'^thanks/$', views.thanks, name='thanks'),
    url(r'^contact/$', views.contact, name='contact'),

)