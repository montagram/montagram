from django.db import models
import os
import uuid


def content_file_name(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'pictures/%s%s' % (uuid.uuid4().hex, ext)


def cropped_file_name(instance, filename):
    f, ext = os.path.splitext(filename)
    return 'crop/%s%s' % (uuid.uuid4().hex, ext)


class Mountain(models.Model):
    name = models.CharField(max_length=100)


class Mountpic(models.Model):
    mount = models.ForeignKey(Mountain, null=True)
    picture = models.ImageField(upload_to=content_file_name)
    croppic = models.ImageField(upload_to=cropped_file_name)
