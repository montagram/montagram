Welcome on Montagram
====================

How to install
--------------

Clone this repository

Set up the virtual environement
	
	$ virtualenv venv

if python 3.x is set up by default, use the following command
	
	$ virtualenv2 venv
	
Acitvate it

	$ source venv/bin/activate

Install the dependancy

	$ pip install -r requirements.txt

For those using apple 3.4 clang version, you first need to change some default flags

    export CFLAGS=-Qunused-arguments
    export CPPFLAGS=-Qunused-arguments

Then run the pip install command with

    $ sudo -E

Setup DATABASE_URL (see https://github.com/kennethreitz/dj-database-url)
	
	$ export DATABASE_URL='postgres://USER@localhost/montagram'
