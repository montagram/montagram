from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'montagram.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'montagram.views.index', name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^reco/', include('reco.urls', namespace="reco")),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),

 )
